import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoursListComponent } from './cours-list/cours-list.component';
import { NewCoursComponent } from './new-cours/new-cours.component';
import { SingleCoursComponent } from './single-cours/single-cours.component';
import { ModifyCoursComponent } from './modify-cours/modify-cours.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  { path: 'new-cours', component: NewCoursComponent, canActivate: [AuthGuard] },
  { path: 'all-cours', component: CoursListComponent, canActivate: [AuthGuard] },
  { path: 'cours/:id', component: SingleCoursComponent, canActivate: [AuthGuard] },
  { path: 'modify-cours/:id', component: ModifyCoursComponent, canActivate: [AuthGuard] },
  { path: 'auth/login', component: LoginComponent },
  { path: 'auth/signup', component: SignupComponent },
  { path: '', pathMatch: 'full', redirectTo: 'auth/login' },
  { path: '**', redirectTo: 'all-cours' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard
  ]
})
export class AppRoutingModule {}
