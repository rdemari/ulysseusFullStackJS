import { Component, OnDestroy, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';
import { CoursService } from '../services/cours.service';
import { Subscription } from 'rxjs';
import { Cours } from '../models/Cours.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cours-list',
  templateUrl: './cours-list.component.html',
  styleUrls: ['./cours-list.component.scss']
})
export class CoursListComponent implements OnInit, OnDestroy {

  public cours: Cours[] = [];
  public part: number;
  public loading: boolean;

  private coursSub: Subscription;
  private partSub: Subscription;

  constructor(private state: StateService,
              private coursService: CoursService,
              private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.state.mode$.next('list');
    this.coursSub = this.coursService.cours$.subscribe(
      (cours) => {
        this.cours = cours;
        this.loading = false;
      }
    );
    this.partSub = this.state.part$.subscribe(
      (part) => {
        this.part = part;
      }
    );
    this.coursService.getCours();
  }

  onProductClicked(id: string) {
      this.router.navigate(['/cours/' + id]);
  }

  ngOnDestroy() {
    this.coursSub.unsubscribe();
    this.partSub.unsubscribe();
  }

}
