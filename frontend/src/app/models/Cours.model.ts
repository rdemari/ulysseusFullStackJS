export class Cours {
  _id: string;
  title: string;
  content: string;
  teacher: string;
  imageUrl: string;
  userId: string;
}
