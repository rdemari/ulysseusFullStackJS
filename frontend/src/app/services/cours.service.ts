import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Cours } from '../models/Cours.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoursService {

  constructor(private http: HttpClient) {}

  private cours: Cours[] = [
    /*{
      _id: '324sdfmoih3',
      title: 'Mon objet',
      description: 'A propos de mon objet',
      imageUrl: 'https://c.pxhere.com/photos/30/d6/photographer_camera_lens_slr_photography_hands-1079029.jpg!d',
      price: 4900,
      userId: 'will'
    },
    {
      _id: '324sdfmoih4',
      title: 'Un autre objet',
      description: 'A propos de mon autre objet',
      imageUrl: 'https://www.publicdomainpictures.net/pictures/10000/velka/1536-1249273362hbHb.jpg',
      price: 2600,
      userId: 'will'
    },*/
  ];
  public cours$ = new Subject<Cours[]>();

  getCours() {
    this.http.get('http://localhost:3000/api/cours').subscribe(
      (cours: Cours[]) => {
        if (cours) {
          this.cours = cours;
          this.emitCours();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  emitCours() {
    this.cours$.next(this.cours);
  }

  getCoursById(id: string) {
    return new Promise((resolve, reject) => {
      this.http.get('http://localhost:3000/api/cours/' + id).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  createNewCours(cours: Cours) {
    return new Promise((resolve, reject) => {
      this.http.post('http://localhost:3000/api/cours', cours).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  modifyCours(id: string, cours: Cours) {
    return new Promise((resolve, reject) => {
      this.http.put('http://localhost:3000/api/cours/' + id, cours).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  deleteCours(id: string) {
    return new Promise((resolve, reject) => {
      this.http.delete('http://localhost:3000/api/cours/' + id).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
}
