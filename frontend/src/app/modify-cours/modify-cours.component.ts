import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StateService } from '../services/state.service';
import { CoursService } from '../services/cours.service';
import { Cours } from '../models/Cours.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modify-cours',
  templateUrl: './modify-cours.component.html',
  styleUrls: ['./modify-cours.component.scss']
})
export class ModifyCoursComponent implements OnInit {

  cours: Cours;
  coursForm: FormGroup;
  loading = false;
  errorMessage: string;
  part: number;

  private partSub: Subscription;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private state: StateService,
              private coursService: CoursService) { }

  ngOnInit() {
    this.loading = true;
    this.coursForm = this.formBuilder.group({
      title: [null, Validators.required],
      content: [null, Validators.required],
      teacher: [null, Validators.required],
      imageUrl: [null, Validators.required]
    });
    this.partSub = this.state.part$.subscribe(
      (part) => {
        this.part = part;
      }
    );
    this.state.mode$.next('form');
    this.route.params.subscribe(
      (params) => {
        this.coursService.getCoursById(params.id).then(
          (cours: Cours) => {
            this.cours = cours;
            this.coursForm.get('title').setValue(this.cours.title);
            this.coursForm.get('content').setValue(this.cours.content);
            this.coursForm.get('teacher').setValue(this.cours.teacher);
            this.coursForm.get('imageUrl').setValue(this.cours.imageUrl);
            this.loading = false;
          }
        );
      }
    );
  }

  onSubmit() {
    this.loading = true;
    const cours = new Cours();
    cours.title = this.coursForm.get('title').value;
    cours.content = this.coursForm.get('content').value;
    cours.teacher = this.coursForm.get('teacher').value;
    cours.imageUrl = this.coursForm.get('imageUrl').value;
    cours._id = new Date().getTime().toString();
    cours.userId = this.cours.userId;
    this.coursService.modifyCours(this.cours._id, cours).then(
      () => {
        this.coursForm.reset();
        this.loading = false;
      
        this.router.navigate(['/all-cours']);
           
      },
      (error) => {
        this.loading = false;
        this.errorMessage = error.message;
      }
    );
  }

}
