import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { StateService } from './services/state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'app';

  constructor(private state: StateService,
    private auth: AuthService) { }

  ngOnInit() {
  this.auth.isAuth$.next(false);
  this.auth.userId = '';
  this.auth.token = '';
  this.state.part$.next(3);
  this.state.part = 3;
  }

  ngOnDestroy() {
  }
}
