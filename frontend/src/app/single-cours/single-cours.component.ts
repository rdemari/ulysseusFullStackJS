import { Component, OnDestroy, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Cours } from '../models/Cours.model';
import { CoursService } from '../services/cours.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-single-cours',
  templateUrl: './single-cours.component.html',
  styleUrls: ['./single-cours.component.scss']
})
export class SingleCoursComponent implements OnInit, OnDestroy {

  public cours: Cours;
  public loading: boolean;
  public userId: string;
  public part: number;

  private partSub: Subscription;

  constructor(private state: StateService,
              private router: Router,
              private route: ActivatedRoute,
              private coursService: CoursService,
              private auth: AuthService) { }

  ngOnInit() {
    this.loading = true;
    this.state.mode$.next('single-cours');
    this.userId = this.auth.userId ? this.auth.userId : 'userID40282382';
    this.route.params.subscribe(
      (params: Params) => {
        this.coursService.getCoursById(params.id).then(
          (cours: Cours) => {
            this.loading = false;
            this.cours = cours;
          }
        );
      }
    );
    this.partSub = this.state.part$.subscribe(
      (part) => {
        this.part = part;
        if (part >= 3) {
          this.userId = this.auth.userId;
        }
      }
    );
  }

  onGoBack() {
    this.router.navigate(['/all-cours']);
  }

  onModify() {
    this.router.navigate(['/modify-cours/' + this.cours._id]);
  }

  onDelete() {
    this.loading = true;
    this.coursService.deleteCours(this.cours._id).then(
      () => {
        this.loading = false;
          this.router.navigate(['/all-cours']);
      }
    );
  }

  ngOnDestroy() {
    this.partSub.unsubscribe();
  }
}
