import { Component, OnDestroy, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Cours } from '../models/Cours.model';
import { CoursService } from '../services/cours.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-new-cours',
  templateUrl: './new-cours.component.html',
  styleUrls: ['./new-cours.component.scss']
})
export class NewCoursComponent implements OnInit, OnDestroy {

  public coursForm: FormGroup;
  public loading = false;
  public part: number;
  public userId: string;
  public errorMessage: string;

  private partSub: Subscription;

  constructor(private state: StateService,
              private formBuilder: FormBuilder,
              private coursService: CoursService,
              private router: Router,
              private auth: AuthService) { }

  ngOnInit() {
    this.state.mode$.next('form');
    this.coursForm = this.formBuilder.group({
      title: [null, Validators.required],
      content: [null, Validators.required],
      teacher: [null, Validators.required],
      imageUrl: [null, Validators.required]
    });
    this.partSub = this.state.part$.subscribe(
      (part) => {
        this.part = part;
      }
    );
    this.userId = this.part >= 3 ? this.auth.userId : 'userID40282382';
  }

  onSubmit() {
    this.loading = true;
    const cours = new Cours();
    cours.title = this.coursForm.get('title').value;
    cours.content = this.coursForm.get('content').value;
    cours.teacher = this.coursForm.get('teacher').value;
    cours.imageUrl = this.coursForm.get('imageUrl').value;
    cours._id = new Date().getTime().toString();
    cours.userId = this.userId;
    this.coursService.createNewCours(cours).then(
      () => {
        this.coursForm.reset();
        this.loading = false;
        this.router.navigate(['all-cours']);
      }
    ).catch(
      (error) => {
        this.loading = false;
        this.errorMessage = error.message;
      }
    );
  }

  ngOnDestroy() {
    this.partSub.unsubscribe();
  }

}
