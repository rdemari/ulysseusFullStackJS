const Cours = require('../models/Cours');
const fs = require('fs');

exports.createCours = (req, res, next) => {
    delete req.body._id;
    const cours = new Cours({
        ...req.body
    });
    cours.save()
        .then(() => res.status(201).json({ message: 'Objet enregistré !'}))
        .catch(error => res.status(400).json({ error }));
};

exports.modifyCours = (req, res, next) => {
    const coursObject = req.file ? 
        {
          ...JSON.parse(req.body.cours),
          imageUrl: `${req.protocol}://${req.get("host")}/images/${req.file.filename}`,
        }
      : { ...req.body };
    Cours.updateOne({ _id: req.params.id }, { ...coursObject, _id: req.params.id })
        .then(() => res.status(200).json({ message: 'Objet modifié' }))
        .catch(error => res.status(400).json({ error }));
};

exports.deleteCours = (req, res, next) => {
    Cours.findOne({ _id: req.params.id })
        .then(cours => {
            const filename = cours.imageUrl.split('/images/')[1];
            fs.unlink(`images/${filename}`, () => {
                Cours.deleteOne({ _id: req.params.id })
                  .then(() => res.status(200).json({ message: "Objet supprimé" }))
                  .catch((error) => res.status(400).json({ error }));
            });
        })
        .catch(error => res.status(500).json({ error }));    
};

exports.getOneCours = (req, res, next) => {
    Cours.findOne({ _id: req.params.id })
        .then(cours => res.status(200).json(cours))
        .catch(error => res.status(404).json({ error }));
};

exports.getAllCours = (req, res, next) => {
    Cours.find()
        .then(cours => res.status(200).json(cours))
        .catch(error => res.status(400).json({ error }));
};