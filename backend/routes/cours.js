const express = require('express');
const router = express.Router();

const coursCtrl = require('../controllers/cours');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');

router.post('/', auth, multer, coursCtrl.createCours);
router.put('/:id', auth, multer, coursCtrl.modifyCours);
router.delete('/:id', auth, coursCtrl.deleteCours);
router.get('/:id', auth, coursCtrl.getOneCours);
router.get('/', auth, coursCtrl.getAllCours);

module.exports = router;