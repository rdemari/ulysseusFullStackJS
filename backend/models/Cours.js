const mongoose = require('mongoose');

const coursSchema = mongoose.Schema({
  title: { type: String, required: true },
  content: { type: String, required: true },
  imageUrl: { type: String, required: true },
  userId: { type: String, required: true },
  teacher: { type: String, required: true },
});

module.exports = mongoose.model('Cours', coursSchema);